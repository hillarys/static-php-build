# README #

EP basetheme for setting up a frontend websites.

### How do I get set up? ###

* Summary of set up

Setup using Gulp, Bower and NPM.

* Configuration

`npm install` - this sets up all the Gulp processes.
`bower install` - install bower plugins

&mdash;

To install new bower libraries, run, `bower install lib --save`

* Deployment instructions

To develop locally setup you hosts file to use `paddle8.test`, then change the `paddle8.test` to your new website. Run `gulp` - this runs Sass watch, Babel with ES6 bundling (this does not run JS concatanation, this is completed on deploy) and BrowserSync.

To go live, run `gulp deploy` - this will minify the CSS and concatanate and minify JS.