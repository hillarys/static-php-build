(function () {
'use strict';

var helper = {
    breakpoint: {
        // 'small': 0,
        // 'medium': 640,
        // 'large': 1025,
        // 'xlarge': 1300,
        // 'xxlarge': 1500,
    },
    touch_support: Modernizr.touch
};

var wait_for_final_event = function () {

    var timers = {};

    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            // don't call this twice without a uniqueID
            uniqueId = Math.random() * 1000;
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
}();

// Plugin Functions

var $ = jQuery.noConflict();

var plugins = {
    init: function init() {

        // Foundation must go first as it is a dependancy
        this.foundation.init();

        this.lazy_sizes.init();
        this.match_height.init();
    },


    foundation: {
        init: function init() {

            $(document).foundation();
        }
    },

    lazy_sizes: {
        init: function init() {

            window.lazySizesConfig = window.lazySizesConfig || {};
            lazySizesConfig.expand = 1500;
            lazySizesConfig.loadHidden = false;
        },
        load: function load() {}
    },

    match_height: {

        init: function init() {

            $('.js-match-height').matchHeight();
        }

    }
};

// Plugin Functions

var $$1 = jQuery.noConflict();

var slideshow = {
    init: function init() {

        this.slideshow.init();
    },


    slideshow: {
        init: function init() {}
    }
};

// Slideshow Functions

var $$2 = jQuery.noConflict();

var scroll_lock = {
    init: function init() {

        this.scroll_lock.init();
    },


    scroll_lock: {
        init: function init() {},
        lock: function lock() {
            var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;


            $$2('.site').css('top', -$$2(window).scrollTop());
            $$2('html').addClass('scroll-lock');

            if (!callback) return;

            callback();
        },
        unlock: function unlock() {

            if ($$2('html').hasClass('scroll-lock')) {

                var reset_scroll = parseInt($$2('.site').css('top')) * -1;

                $$2('html').removeClass('scroll-lock');
                $$2('.site').css('top', 'auto');
                $$2('html, body').animate({
                    scrollTop: reset_scroll
                }, 0);
            }
        }
    }
};

// Class import
// import Example from './examples';
// Var import
(function ($, document) {

    $(document).ready(function () {

        app_init.ready();
    });

    $(window).load(function () {});

    $(window).scroll(function () {});

    $(window).resize(function () {

        wait_for_final_event(function () {}, 300, 'init');
    });

    var app_init = {

        ready: function ready() {

            // Scroll Lock
            scroll_lock.init();

            // Plugins
            plugins.init();

            // Slideshow
            slideshow.init();

            // App
            app.init();
        }

        // App Functions

    };var app = {

        init: function init() {

            this.mobile_menu.init();
            this.scroll_to.init();
            this.tabs.init();
        },

        mobile_menu: {
            init: function init() {

                $('.l-header__burger').on('click', function () {

                    if ($('body').hasClass('menu-active')) {

                        $('body').removeClass('menu-active');
                    } else {

                        $('body').addClass('menu-active');
                    }

                    return false;
                });
            }
        },

        scroll_to: {
            init: function init() {

                $('.hero__scroll-down').on('click', function () {

                    $('html, body').animate({
                        scrollTop: $('.js-scroll-to').offset().top - $('.l-header').outerHeight()
                    }, 500);

                    return false;
                });
            }
        },

        tabs: {
            init: function init() {

                $('.tabs__tab').on('click', function () {

                    if ($(this).hasClass('is-active')) {

                        $('.tabs__tab').removeClass('is-active');
                        $('.tab__hidden').removeClass('is-visible');
                    } else {

                        var this_data = $(this).attr('data-tab');

                        $('.tabs__tab').removeClass('is-active');
                        $(this).addClass('is-active');

                        $('.tab__hidden').removeClass('is-visible');
                        $('.tab__hidden[data-tab="' + this_data + '"]').addClass('is-visible');
                    }

                    return false;
                });
            }
        }
    };
})(jQuery, document);

}());
