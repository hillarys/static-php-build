export { plugins };

// Plugin Functions

let $ = jQuery.noConflict();

let plugins = {

    init () {

        // Foundation must go first as it is a dependancy
        this.foundation.init();

        this.lazy_sizes.init();
        this.match_height.init();

    },

    foundation : {

        init () {

            $( document ).foundation();

        }

    },

    lazy_sizes : {

        init () {

            window.lazySizesConfig = window.lazySizesConfig || {};
            lazySizesConfig.expand = 1500;
            lazySizesConfig.loadHidden = false;

        },

        load () {
        }

    },

    match_height : {

        init : function() {

            $( '.js-match-height' ).matchHeight();

        }

    }
}