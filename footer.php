    </main>
    <?php // Close Main ?>

    <?php

        // Start Footer
        include( 'partials/footer.php' );
        // End Footer

    ?>

    </div>
    <?php // Close Wrapper ?>
</div>
<?php // Close App ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

    <?php if( strstr( $_SERVER['HTTP_HOST'], "localhost" ) === "localhost" ) { ?>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.core.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.interchange.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.triggers.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.mediaQuery.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.timerAndImageLoader.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.js"></script>
        <script src="node_modules/foundation-sites/dist/js/plugins/foundation.util.motion.js"></script>
        <script src="node_modules/jquery-match-height/dist/jquery.matchHeight.js"></script>
        <script src="node_modules/flickity/dist/flickity.pkgd.js"></script>
        <script src="node_modules/lazysizes/lazysizes.js"></script>
        <script src="node_modules/lazysizes/plugins/object-fit/ls.object-fit.js"></script>
        <script src="node_modules/protonet/jquery.inview/jquery.inview.js"></script>
        <script src="assets/js/libs/modernizr.js"></script>
        <script src="assets/js/libs/velocity.js"></script>
        <script src="assets/js/dist/global.js"></script>
    <?php } else { ?>
        <script src="assets/js/main.js?<?php echo filemtime( __DIR__ . '/assets/js/main.js' ); ?>"></script>
    <?php } ?>
</body>
</html>