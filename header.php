<?php

    // Template Parts
    require_once( 'inc/classes/class-lazy-image.php' );
    require_once( 'inc/classes/class-modules.php' );
    // require_once( 'inc/classes/class-slideshows.php' );

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

    <?php

    // Start Meta
    include( 'partials/meta.php' );
    // End Meta

    ?>

    <title>Hillarys™ Made-to-Measure Blinds, Curtains &amp; Shutters</title>

    <!-- Hillarys stylesheet -->
    <link rel="stylesheet" href="//static.hillarys.co.uk/build/styles/main.css" media="all">
    
    <!-- Custom stylesheet -->
    <link rel="stylesheet" href="/assets/css/main.css?<?php echo filemtime( __DIR__ . '/assets/css/main.css' ); ?>" type="text/css" media="all" />

</head>

<body>

    <div class="hide"><?php include( 'assets/img/icons/svg-symbols.svg' ); ?></div>

    <?php // Open App ?>
    <div id="app" class="site">

        <?php // Open Wrapper ?>
        <div id="page_wrapper">

        <?php

        // Start Header
        include( 'partials/header.php' );
        // End Header

        ?>

        <?php // Open Main ?>
        <main class="l-main">