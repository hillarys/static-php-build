// Processes
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    gcmq = require('gulp-group-css-media-queries'),
    svg2png = require('gulp-svg2png'),
    svgSymbols = require('gulp-svg-symbols'),
    babel = require('rollup-plugin-babel'),
    rollup = require('gulp-rollup');

// Config
var config = {}

config.local_url = 'localhost:8888';
// config.local_url = 'localhost';

// Paths
config.paths = {}

config.paths.src = {
    styles: './assets/css/scss/**/*.scss',
    js : './assets/js/main.js',
    babel: './assets/js/src/*.js',
    svg: 'assets/img/icons/svg/**/*.svg'
}

config.paths.dist = {
    styles: './assets/css',
    maps: './assets/css',
    js: './assets/js/',
    transpiled: './assets/js/dist',
    svg: 'assets/img/icons/',
    png: 'assets/img/icons/png/'
}

// Options
var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

var autoPrefixerOptions = {
    browsers: ['last 10 versions', '> 5%', 'Firefox ESR']
};

// Tasks
gulp.task('sass', function () {
    return gulp
        .src( config.paths.src.styles )
        .pipe(sass(sassOptions)).on('error', notify.onError(function (error) {
            return "Problem file -> " + error.message;
        }))

        // Prefix that shit
        .pipe(autoprefixer())
        .pipe(gulp.dest( config.paths.dist.styles ))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
    return gulp.src([
        // This adds all files except main
        './assets/js/libs/*.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.core.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.interchange.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.util.mediaQuery.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.util.timerAndImageLoader.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.util.motion.js',
        './node_modules/foundation-sites/dist/js/plugins/foundation.util.triggers.js',
        './node_modules/jquery-match-height/dist/jquery.matchHeight-min.js',
        './node_modules/flickity/dist/flickity.pkgd.js',
        './node_modules/lazysizes/plugins/object-fit/ls.object-fit.js',
        './node_modules/lazysizes/lazysizes.js',
        './assets/js/dist/*.js'
        ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest( config.paths.dist.js ));
});

gulp.task('compress', function() {
    return gulp.src( config.paths.src.js )
        .pipe(uglify())
        .pipe(gulp.dest( config.paths.dist.js ));
});

gulp.task('watch', function() {
    gulp.watch( config.paths.src.styles, ['sass'] );
    gulp.watch( config.paths.src.babel , ['bundle']);
});

gulp.task('bundle', function() {
  gulp.src('./assets/js/src/**/*.js')
    // transform the files here.
    .pipe(rollup({
      // any option supported by Rollup can be set here.
      "format": "iife",
      "plugins": [
        require("rollup-plugin-babel")({
          "presets": [["es2015", { "modules": false }]],
          "plugins": ["external-helpers"]
        })
      ],
      input: './assets/js/src/global.js'
    })).on('error', notify.onError(function (error) {
            return "Problem file -> " + error.message;
        }))

    .pipe(gulp.dest('./assets/js/dist'));
});

gulp.task('sass-compress', function () {
    return gulp
        .src( config.paths.src.styles )
        .pipe(sass())
        // @TODO: This changes the order with Foundation Media Queries
        // To investigate...
        // .pipe(gcmq())
        .pipe(autoprefixer(autoPrefixerOptions))
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest( config.paths.dist.styles ));
});

gulp.task('svg', function () {
    return gulp
        .src( config.paths.src.svg )
        .pipe( svg2png() )
        .pipe( gulp.dest( config.paths.dist.png ) )
});

gulp.task('sprites', ['svg'], function () {
    return gulp
        .src( config.paths.src.svg )
        .pipe(
            svgSymbols({
                className: '.icon--%f'
            })
        )
        .pipe( gulp.dest( config.paths.dist.svg ) )
});

gulp.task('sync', function() {
    browserSync.init({
        open: false,
        proxy: config.local_url
    });
});

gulp.task('default', ['sync', 'watch']);
gulp.task('deploy', ['bundle', 'sass-compress', 'scripts']);