<?php

    namespace Hillarys;

    if ( ! defined( 'ABSPATH' ) ) exit;

    /**
     * Demo Class.
     *
     * @class       Hillarys\Demo
     * @version     1.0.0
     */

    class Demo {

        public function __construct() {

        }

        public static function function_name() {

        }

    }

    // Call
    // Hillarys\Demo::function_name();