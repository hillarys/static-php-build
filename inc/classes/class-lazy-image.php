<?php

    namespace Hillarys;

    /**
     * Images Class.
     *
     * @class       Hillarys\Images
     * @version     1.0.0
     */

    class Images {

        public function __construct() {

        }

        /**
         * Get Icon
         * Uses sprites map
         *
         * @param str $name
         * @param num $vb_w
         * @param num $vb_h
         * @return str
         */
        public static function get_icon( $name = false, $vb_w = 100, $vb_h = 100 ) {

            if( !$name )
                return false;

            $svg = '<svg viewBox="0 0 ' . $vb_w . ' ' . $vb_h . '" role="presentation" class="icon--' . $name . '">:<use xlink:href="#' . $name . '"></use></svg>';

            return $svg;

        }

        /**
         * Get Image
         *
         * @param str $aspect_ratio
         * @return str
         */
        public static function get_image( $src = '', $width = '', $height = '', $alt = '#', $src_mobile = '' ) {

            $image_path = '/assets/img/tmp/' . $src;
            $aspect_percentage = $height / $width * 100;

            if( $src_mobile ) {
                $src_mobile = '/assets/img/tmp/' . $src_mobile;
            } else {
                $src_mobile = '/assets/img/tmp/' . $src;
            }

            $extension = pathinfo( $src , PATHINFO_EXTENSION );

            if( $extension == 'png' || $extension == 'svg' ) {
                $modifier = 'image--bg-transparent';
            }

            return '<div class="intrinsic-image ' . $modifier . '" style="padding-bottom: ' . $aspect_percentage . '%;"><img data-src="' . $image_path . '" data-srcset="' . $src_mobile . ' 300w, ' . $image_path . ' 600w, ' . $image_path . ' 900w" class="lazyload" alt="' . $alt . '" /></div>';

        }

    }

    // Call
    // echo Hillarys\Images::get_intrinsic_image( $image, 'large' );
    // new Images();