<!-- Favicons -->
<link rel="icon" type="image/x-icon" href="https://www.hillarys.co.uk/favicon.ico">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_72x72.png" sizes="72x72">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_48x48.png" sizes="48x48">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_144x144.png" sizes="144x144">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_168x168.png" sizes="168x168">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_384x384.png" sizes="384x384">
<link rel="icon" type="image/png" href="https://static.hillarys.co.uk/build/global/favicon_red_512x512.png" sizes="512x512">

<!-- Apple Icons -->
<meta name="apple-mobile-web-app-title" content="Hillarys">
<link rel="apple-touch-icon" sizes="57x57" href="https://static.hillarys.co.uk/build/global/favicon_red_57x57.png">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://static.hillarys.co.uk/build/global/favicon_red_57x57.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://static.hillarys.co.uk/build/global/favicon_red_72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://static.hillarys.co.uk/build/global/favicon_red_76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://static.hillarys.co.uk/build/global/favicon_red_114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://static.hillarys.co.uk/build/global/favicon_red_120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://static.hillarys.co.uk/build/global/favicon_red_144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://static.hillarys.co.uk/build/global/favicon_red_152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://static.hillarys.co.uk/build/global/favicon_red_180x180.png">
<link rel="mask-icon" href="https://static.hillarys.co.uk/build/global/favicon_red.svg" color="#ec0142 ">

<!-- MS Tile Icons -->
<meta name="application-name" content="Hillarys">
<meta name="msapplication-TileColor" content="#ec0142">
<meta name="msapplication-TileImage" content="https://static.hillarys.co.uk/build/global/favicon_red_144x144.png">
<meta name="msapplication-square70x70logo" content="https://static.hillarys.co.uk/build/global/favicon_red_70x70.png" />
<meta name="msapplication-square150x150logo" content="https://static.hillarys.co.uk/build/global/favicon_red_150x150.png" />
<meta name="msapplication-wide310x150logo" content="https://static.hillarys.co.uk/build/global/favicon_red_310x150.png" />
<meta name="msapplication-square310x310logo" content="https://static.hillarys.co.uk/build/global/favicon_red_310x310.png" />
<meta name="msapplication-tooltip" content="Hillarys">
<meta name="msapplication-config" content="https://www.hillarys.co.uk/browserconfig.xml">
<meta name="theme-color" content="#ec0142">